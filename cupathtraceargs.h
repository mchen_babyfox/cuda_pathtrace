#pragma once

#include <string.h>
#include "optionparser.h"

struct cuPathTraceArg: option::Arg
{
	enum OPTINDEX {
		WIDTH = 0, //image width
		HEIGHT, //image height
		RENDERSCALE, //render buffer scale relative to image resolution
		PATHLENGTH, //length of a light path
		SAMPLES, //number of samples
		NODISPLAY, //no display window
		OUTPUTFILE, //output file name
		UNKNOWN
	};

	static char Filename[256];
	static int Width;
	static int Height;
	static int Samples;
	static int PathLength;
	static int NoDisplay;
	static float RenderScale;

	static option::ArgStatus ParseArgument(const option::Option& option, bool msg);
	
	static option::ArgStatus NoArgument(const option::Option& option, bool msg);

	static bool ParseCmdline(int argc, const char** argv);
};

char cuPathTraceArg::Filename[256];
int cuPathTraceArg::Width = 1280;
int cuPathTraceArg::Height = 720;
int cuPathTraceArg::Samples = 500;
int cuPathTraceArg::PathLength = 5;
float cuPathTraceArg::RenderScale = 1.0f;
int cuPathTraceArg::NoDisplay = 0;

option::ArgStatus cuPathTraceArg::ParseArgument(const option::Option& option, bool msg)
{

	if (option.arg == NULL)
	{
		return option::ARG_ILLEGAL;
	}

	if (option.index() == WIDTH)
	{
		Width = atoi(option.arg);

		return option::ARG_OK;
	}
	else if (option.index() == HEIGHT)
	{
		Height = atoi(option.arg);

		return option::ARG_OK;
	}
	else if (option.index() == SAMPLES)
	{
		Samples = atoi(option.arg);

		return option::ARG_OK;
	}
	else if (option.index() == RENDERSCALE)
	{
		RenderScale = atof(option.arg);

		return option::ARG_OK;
	}
	else if (option.index() == PATHLENGTH)
	{
		RenderScale = atof(option.arg);

		return option::ARG_OK;
	}
	else if (option.index() == OUTPUTFILE)
	{
		sprintf(Filename, "%s.ppm", option.arg);

		return option::ARG_OK;
	}
	return option::ARG_ILLEGAL;
}

option::ArgStatus cuPathTraceArg::NoArgument(const option::Option& option, bool msg)
{
	if(option.index() == NODISPLAY)
	{
		NoDisplay = 1;
	}

	return option::ARG_OK;
}

bool cuPathTraceArg::ParseCmdline(int argc, const char** argv)
{
	const option::Descriptor usage[] = {
		{ cuPathTraceArg::WIDTH, 0, "w", "width", cuPathTraceArg::ParseArgument, "--width: Width of the image in pixels" },
		{ cuPathTraceArg::HEIGHT, 0, "h", "height", cuPathTraceArg::ParseArgument, "--height: Height of the image in pixels" },
		{ cuPathTraceArg::SAMPLES, 0, "n", "nsample", cuPathTraceArg::ParseArgument, "--nsample: Number of sample per pixel" },
		{ cuPathTraceArg::PATHLENGTH, 0, "l", "plength", cuPathTraceArg::ParseArgument, "--plength: Length of each path" },
		{ cuPathTraceArg::RENDERSCALE, 0, "r", "renscale", cuPathTraceArg::ParseArgument, "--renscale: Scale of the rendering buffer relative to the image resolution" },
		{ cuPathTraceArg::NODISPLAY, 0, "", "nodisplay", cuPathTraceArg::NoArgument, "--nodisplay: Run path trace without display" },
		{ cuPathTraceArg::OUTPUTFILE, 0, "o", "", cuPathTraceArg::ParseArgument, "-o: Specify output file" },
		{ 0,0,0,0,0,0 }
	};

	sprintf(Filename, "%s.ppm", "image");
	
	option::Stats stats(usage, argc - 1, &argv[1]);
	//printf("%d\n", stats.options_max);
	//printf("%d\n", stats.buffer_max);
	option::Option *options = new option::Option[stats.options_max];
	option::Option *buffer = new option::Option[stats.buffer_max];
	option::Parser parse(usage, argc - 1, &argv[1], options, buffer);

	if (parse.error())
	{
		option::printUsage(std::cout, usage);
		return false;
	}
	
	delete[] options;
	delete[] buffer;

	return true;
}
