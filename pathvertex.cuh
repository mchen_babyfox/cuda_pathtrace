#pragma once

#include "vector3f.cuh"

struct PathVertex
{
	public:
		//Vector3 position;
		Vector3 emissive;
		Vector3 colour;
};
