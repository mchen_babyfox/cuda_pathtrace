#include <stdio.h>
#include <string.h>
#include "objfilereader.h"

static unsigned long hash(char *str)
{
	unsigned long hash = 5381;
	int c;

	while (c = *str++)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

static int findMaterialIndex(unsigned long id, const Material *material, int numMaterials)
{
	for (int i = 0; i < numMaterials; i++)
	{
		if ((material + i)->Name == id)
			return i;
	}

	return -1;
}

static int firstPassOBJRead(const char* filename, int* vertex_count, int* vert_normal_count, int* vert_texcoord_count, int* triangle_count)
{
	FILE* pfile;

	*vertex_count = 0;
	*vert_normal_count = 0;
	*vert_texcoord_count = 0;
	*triangle_count = 0;

#if defined(_WINDOWS) || defined(_WIN32)
	fopen_s(&pfile, filename, "r");
#else
	pfile = fopen(filename, "r");
#endif

	if (!pfile)
	{
		//something has gone wrong when opening the file.
		return 1;
	}

	char tempbuffer[1024];
	char* strread = fgets(tempbuffer, 1024, pfile);

	do
	{
		char* v = &(tempbuffer[0]);
		if (*v == 'v')
		{
			if (*(v + 1) == 'n')		
				*vert_normal_count += 1;
			else if (*(v + 1) == 't')	
				*vert_texcoord_count += 1;
			else					
				*vertex_count += 1;
		}
		else if (*v == 'f')
		{
			int numIndices = 0;
			char* vs = strstr(v, " ");

			//vs = strstr(vs+1, " ");
			while (vs)
			{
				//numIndices += 1;
				vs = strstr(vs + 1, "/");
				if (vs)
					numIndices += 1;
			}

			int numTriangles = 1;//numIndices / 2 - 2;
			*triangle_count += numTriangles;
		}

		strread = fgets(tempbuffer, 1024, pfile);

	} while (strread != NULL);

	fclose(pfile);

	return 0;
}

static int secondPassOBJRead(const char* filename, int nVerts, int nNormals, int nTexcoords, Triangle* mesh, const Material *materials, int nMaterials)
{
	FILE* pfile;
#if defined(_WINDOWS) || defined(_WIN32)
	fopen_s(&pfile, filename, "r");
#else
	pfile = fopen(filename, "r");
#endif

	if (!pfile)
	{
		//something has gone wrong when opening the file.
		return 1;
	}

	char tempbuffer[1024];
	char* strread = fgets(tempbuffer, 1024, pfile);
	int vertex_read = 0;
	int normal_read = 0;
	int texcoord_read = 0;
	int triangle_read = 0;
	int matIndex = -1;

	Vector3 *normal_buffer = new Vector3[nNormals];
	Vector3 *vertex_buffer = new Vector3[nVerts];
	Vector3 *texcoord_buffer = new Vector3[nTexcoords];

	do
	{
		char* v = &(tempbuffer[0]);
		float x, y, z;

		if (*v == 'v')
		{
			if (*(v + 1) == 'n')
			{

#if defined(_WINDOWS) || defined(_WIN32)
				sscanf_s(v, "%*s %f %f %f", &x, &y, &z);
#else
				sscanf(v, "%*s %f %f %f", &x, &y, &z);
#endif
				(normal_buffer + normal_read)->v.x = x;
				(normal_buffer + normal_read)->v.y = y;
				(normal_buffer + normal_read)->v.z = z;

				normal_read += 1;
			}
			else if (*(v + 1) == 't')
			{
#if defined(_WINDOWS) || defined(_WIN32)
				sscanf_s(v, "%*s %f %f", &x, &y);
#else
				sscanf(v, "%*s %f %f", &x, &y);
#endif
				(texcoord_buffer + texcoord_read)->v.x = x;
				(texcoord_buffer + texcoord_read)->v.y = y;
				texcoord_read += 1;
			}
			else
			{
#if defined(_WINDOWS) || defined(_WIN32)
				sscanf_s(v, "%*s %f %f %f", &x, &y, &z);
#else
				sscanf(v, "%*s %f %f %f", &x, &y, &z);
#endif
				(vertex_buffer + vertex_read)->v.x = x;
				(vertex_buffer + vertex_read)->v.y = y;
				(vertex_buffer + vertex_read)->v.z = z;

				vertex_read += 1;
			}
		}
		else if (*v == 'f')
		{
			int index1, index2, index3;
			int nidx1, nidx2, nidx3;
			int tidx1, tidx2, tidx3;
			mesh[triangle_read].MaterialIndex = matIndex;
#if defined(_WINDOWS) || defined(_WIN32)
			if (sscanf_s(v, "%*s %d/%d/%d %d/%d/%d %d/%d/%d", &index1, &tidx1, &nidx1,
				&index2, &tidx2, &nidx2,
				&index3, &tidx3, &nidx3) == 9)
#else	
			if (sscanf(v, "%*s %d/%d/%d %d/%d/%d %d/%d/%d", &index1, &tidx1, &nidx1,
				&index2, &tidx2, &nidx2,
				&index3, &tidx3, &nidx3) == 9)
#endif

			{
				mesh[triangle_read].Vertices[0].Position = vertex_buffer[index1 - 1];
				mesh[triangle_read].Vertices[1].Position = vertex_buffer[index2 - 1];
				mesh[triangle_read].Vertices[2].Position = vertex_buffer[index3 - 1];
			}
#if defined(_WINDOWS) || defined(_WIN32)
			else if (sscanf_s(v, "%*s %d/%d %d/%d %d/%d", &index1, &tidx1, &index2, &tidx2, &index3, &tidx3) == 6) //f v/vt v/vt v/vt
#else
			else if (sscanf(v, "%*s %d/%d %d/%d %d/%d", &index1, &tidx1, &index2, &tidx2, &index3, &tidx3) == 6) //f v/vt v/vt v/vt
#endif
			{
				mesh[triangle_read].Vertices[0].Position = vertex_buffer[index1 - 1];
				mesh[triangle_read].Vertices[1].Position = vertex_buffer[index2 - 1];
				mesh[triangle_read].Vertices[2].Position = vertex_buffer[index3 - 1];
			}
#if defined(_WINDOWS) || defined(_WIN32)
			else if (sscanf_s(v, "%*s %d//%d %d//%d %d//%d", &index1, &tidx1, &index2, &tidx2, &index3, &tidx3) == 6) //f v//vn v//vn v//vn
#else
			else if (sscanf(v, "%*s %d//%d %d//%d %d//%d", &index1, &tidx1, &index2, &tidx2, &index3, &tidx3) == 6) //f v//vn v//vn v//vn
#endif
			{
				mesh[triangle_read].Vertices[0].Position = vertex_buffer[index1 - 1];
				mesh[triangle_read].Vertices[1].Position = vertex_buffer[index2 - 1];
				mesh[triangle_read].Vertices[2].Position = vertex_buffer[index3 - 1];
			}
#if defined(_WINDOWS) || defined(_WIN32)
			else if (sscanf_s(v, "%*s %d %d %d", &index1, &index2, &index3) == 3)
#else
			else if (sscanf(v, "%*s %d %d %d", &index1, &index2, &index3) == 3)
#endif
			{
				mesh[triangle_read].Vertices[0].Position = vertex_buffer[index1 - 1];
				mesh[triangle_read].Vertices[1].Position = vertex_buffer[index2 - 1];
				mesh[triangle_read].Vertices[2].Position = vertex_buffer[index3 - 1];
			}
			else
			{
				//something is seriously fucked. abort
				//I don't want to handle them
				break;
			}
			triangle_read += 1;
		}
		else if (char *s = strstr(tempbuffer, "usemtl"))
		{
			char tmp[64];
			unsigned long name;
#if defined(_WINDOWS) || defined(_WIN32)
			sscanf_s(s, "usemtl %63s", tmp, sizeof(tmp));
#else
			sscanf(s, "usemtl %63s", tmp, sizeof(tmp));
#endif
			name = hash(tmp);
			matIndex = findMaterialIndex(name, materials, nMaterials);
		}

		strread = fgets(tempbuffer, 1024, pfile);

	} while (strread != NULL);

	fclose(pfile);

	delete[] vertex_buffer;
	delete[] normal_buffer;
	delete[] texcoord_buffer;

	return 0;
}

int importOBJMesh(const char* filename, Triangle** triangles, const Material *materials, int nMaterials)
{
	int num_triangles = 0;
	int num_vertices = 0;
	int num_normals = 0;
	int num_texcoords = 0;

	firstPassOBJRead(filename, &num_vertices, &num_normals, &num_texcoords, &num_triangles);

	*triangles = new Triangle[num_triangles];

	secondPassOBJRead(filename, num_vertices, num_normals, num_texcoords, *triangles, materials, nMaterials);

	return num_triangles;
}

static int firstPassMTLRead(const char *filename, int &num_materials)
{
	FILE *pFile;

#if defined(_WINDOWS) || defined(_WIN32)
	fopen_s(&pFile, filename, "r");
#else
	pFile = fopen(filename, "r");
#endif

	do
	{
		char buffer[512];
		char *strread = fgets(buffer, 512, pFile);

		if (strstr(buffer, "newmtl"))
		{
			num_materials = num_materials + 1;
		}

	} while (!feof(pFile));

	fclose(pFile);

	return 0;
}

static int secondPassMTLRead(const char *filename, Material *materials, int num_materials)
{
	FILE *pFile;

#if defined(_WINDOWS) || defined(_WIN32)
	fopen_s(&pFile, filename, "r");
#else
	pFile = fopen(filename, "r");
#endif

	int nRead = 0;
	
	char buffer[512];
	char name[64];

	do
	{
		char *strread = fgets(buffer, 512, pFile);
		float x, y, z;
#if defined(_WINDOWS) || defined(_WIN32)
		if (sscanf_s(buffer, "newmtl %63s\n", name, sizeof(name)) == 1)
#else
		if (sscanf(buffer, "newmtl %63s\n", name, sizeof(name)) == 1)
#endif
		{
			nRead = nRead + 1;
			materials[nRead - 1].Name = hash(name);
		}
		else if (char *s = strstr(buffer, "map_"))
		{

		}
		else if (char *s = strstr(buffer, "Ka"))
		{
#if defined(_WINDOWS) || defined(_WIN32)
			sscanf_s(s, "Ka %f %f %f", &x, &y, &z);
#else	
			sscanf(s, "Ka %f %f %f", &x, &y, &z);
#endif
			materials[nRead - 1].Ka = Vector3(x, y, z);
		}
		else if (char *s = strstr(buffer, "Kd"))
		{

#if defined(_WINDOWS) || defined(_WIN32)
			sscanf_s(s, "Kd %f %f %f", &x, &y, &z);
#else
			sscanf(s, "Kd %f %f %f", &x, &y, &z);
#endif
			materials[nRead - 1].Kd = Vector3(x, y, z);
		}
		else if (char *s = strstr(buffer, "Ks"))
		{
#if defined(_WINDOWS) || defined(_WIN32)
			sscanf_s(s, "Ks %f %f %f", &x, &y, &z);
#else
			sscanf(s, "Ks %f %f %f", &x, &y, &z);
#endif
			materials[nRead - 1].Ks = Vector3(x, y, z);
		}
		else if (char *s = strstr(buffer, "Ke"))
		{
#if defined(_WINDOWS) || defined(_WIN32)
			sscanf_s(s, "Ke %f %f %f", &x, &y, &z);
#else
			sscanf(s, "Ke %f %f %f", &x, &y, &z);
#endif
			materials[nRead - 1].Ke = Vector3(x, y, z);
		}
	} while (!feof(pFile));

	fclose(pFile);

	return 0;
}

int importMTL(const char* filename, Material** materials)
{
	int num_materials = 0;

	firstPassMTLRead(filename, num_materials);

	*materials = new Material[num_materials];

	secondPassMTLRead(filename, *materials, num_materials);

	return num_materials;
}
