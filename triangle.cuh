#pragma once

#include "vector3f.cuh"
#include "ray.cuh"

struct Vertex
{
	public:
		Vector3	Position;
		
		__host__ __device__ Vertex() {;}
		__host__ __device__ Vertex(const Vector3 &pos):
			Position(pos) {;}
};

struct Triangle
{
	public:
		Vertex Vertices[3];
		//Vector3 Normal;
		int MaterialIndex;

		__host__ __device__ Triangle() {;}
		
		__host__ __device__ Triangle(const Vertex &v0, const Vertex &v1, const Vertex &v2)
		{
			Vertices[0] = v0;
			Vertices[1] = v1;
			Vertices[2] = v2;
		}

		__device__ float intersect(const Ray *r);

};
