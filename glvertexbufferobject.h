#pragma once

#include <cuda_runtime.h>

struct GLVertexBufferObject
{
	private:
		void FreeBuffer();

	public:
		GLuint vbo;

		GLVertexBufferObject():
			vbo(-1){;}

		~GLVertexBufferObject();

		void CreateVBO(size_t bytes, void *data);
		void RegisterCudaGraphicsResource(cudaGraphicsResource **pRes, unsigned int gfx_res_flags);
};
