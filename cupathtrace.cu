#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <cuda_runtime.h>
#include <curand_kernel.h>
#include "cuda_call.h"
#include "vector3f.cuh"
#include "cupathtrace.cuh"
#include "sphere.cuh"
#include "brdfparams.cuh"

#if 1
__device__ bool intersect(Ray* ray, float* t, Sphere* spheres, int n, int* result)
{
	float d = 0.0f;
	float inf = 1e5;
	*t = inf;

	if(ray->d.norm() < 0.1f)
		return false;

#pragma unroll 5
	for (int i =0; i < n; i++)
	{
		d = spheres[i].intersect(ray);
		if( d && d < *t)
		{
			*t = d;
			*result = i;
		}
	}
	return *t<inf;
}
#endif

#if 1
__device__ bool intersect(Ray* ray, float* t, Triangle* triangles, int n, Triangle **result)
{
	float d = 0.0f;
	float inf = 1e5;
	*t = inf;
	*result = NULL;
#pragma unroll 5
	for (int i = 0; i < n; i++)
	{
		d = (triangles+i)->intersect(ray);
		if (d && d < *t)
		{
			*t = d;
			*result = triangles+i;
		}
	}
	return *t<inf;
}
#endif

__device__ Vector3 brdf_phong(const Vector3* wi, const Vector3* wo, const Sphere* sphere)
{
	return Vector3();
}

__device__ Vector3 brdf_diffuse(const Vector3* wi, const Vector3* wo, const Sphere* sphere)
{
	return Vector3();
}

#if 1
__device__ Vector3 radiance(Ray* inRay, BRDFParams* brdf, Sphere* spheres, PrecomputedBRDF* pbrdf, curandState* state, int& obj_id, int niter)
{
	Vector3 f;	
	float t;

	bool hit = intersect(inRay, &t,  spheres, 11, &obj_id); 
	
	if(!hit) 
	{
		//*brdf = f;
		return f;
	}
	
	Sphere* obj = &spheres[obj_id];
	
	Vector3 x = inRay->o + inRay->d*t;
	Vector3 n = (x - obj->position);
	n.normalise();
	Vector3 nl = n.dot(inRay->d) < 0.0 ? n : n*-1.0f;
	f = obj->colour;

	brdf->fd = f;
	
	//float4 r = curand_uniform4(state);
	float2 r;
	r.x = curand_uniform(state);
	r.y = curand_uniform(state);
	//r.z = curand_uniform(state);
	//r.w = curand_uniform(state);

	float r1 = 2.0f*M_PI*r.x;
	float r2 = r.y;
	float r2s = __fsqrt_ru(r2);
	
	Vector3 w = nl;
	Vector3 u = fabs(w[0])>.1 ? Vector3(0,1) : Vector3(1);
	u = u%w;
	u.normalise();
	Vector3 v = w%u;
	Vector3 d = u*cos(r1)*r2s + v*sin(r1)*r2s + w*__fsqrt_ru(1.0f-r2);
	d.normalise();
	
	Vector3 ut(u[0],v[0],w[0]);
	Vector3 vt(u[1],v[1],w[1]);
	Vector3 wt(u[2],v[2],w[2]);

	Vector3 ray(-inRay->d.dot(ut), -inRay->d.dot(vt), -inRay->d.dot(wt));

	float theta_in = acosf(ray[2]);
	float phi_in = M_PI + atan2f(ray[1],ray[0]);
	//Vector3 s(5, 5, 5);
	//Vector3 refl_vec = d.reflect(w);
	//float phong = refl_vec.dot(inRay->d);

	//*brdf = obj->colour;// *d.dot(w);// + s*powf(phong,50);

	if(obj->refl == BRDF)
	{
		brdf->wd.d = d;
		brdf->wd.o = x;

		brdf->fd = pbrdf->LookupBRDF(asinf(r2s),r1,theta_in, phi_in);
		//brdf->fd = pbrdf->LookupBRDF(theta_in, phi_in, asinf(r2s),r1);
		//brdf->fd.v.x = brdf->fd.v.x > 1.0 ? 1.0 : brdf->fd.v.x;
		//brdf->fd.v.y = brdf->fd.v.y > 1.0 ? 1.0 : brdf->fd.v.y;
		//brdf->fd.v.z = brdf->fd.v.z > 1.0 ? 1.0 : brdf->fd.v.z;

		return obj->emissive;
	}

	if (obj->refl == DIFFUSE)
	{
		//inRay->d = d;
		//inRay->o = x;
		brdf->wd.d = d;
		brdf->wd.o = x;

		return obj->emissive;
	}

	if (obj->refl == SPECULAR || obj->refl == REFRACTIVE)
	{
		d = inRay->d.reflect(n);
		brdf->wr.d = d;
		brdf->wr.o = x;
		brdf->fr = f;
	}

	//float max_refl = (*brdf)[0] > (*brdf)[1] && (*brdf)[0] > (*brdf)[2] ? (*brdf)[0] : (*brdf)[1] > (*brdf)[2] ? (*brdf)[1] : (*brdf)[2];

	if (obj->refl == REFRACTIVE)
	{
		float into = n.dot(nl) > 0 ? 1.0f : -1.0f;
		float nt = 1.5f;
		float nc = 1.0f;
		float nnt = into > 0.0f ? nc/nt : nt/nc;
		float ddn = inRay->d.dot(nl);
		float cos2t = 1.0f - nnt*nnt*(1.0f - ddn*ddn);

		if(cos2t < 0.0f){
			//inRay->o = x;
			//inRay->d = d;
			return obj->emissive;
		}
				
		d = inRay->d*nnt - n*into*(ddn*nnt+__fsqrt_ru(cos2t));
		d.normalise();

		float a = nt - nc;
		float b = nt + nc;
		float R0 = a*a / (b*b);
		float c = 1.0f - (into > 0.0f? -ddn : d.dot(n));
		float Re = R0 + (1.0f - R0)*c*c*c*c*c;
		float Tr = 1.0f - Re;
		float P = .25f + .5f*Re;
		float RP = Re / P;
		float TP = Tr / (1.0f - P);

		float rou = curand_uniform(state);
		
		if ( niter < 2 )
		{
			brdf->fr = Re;
			brdf->ft = Tr;
			//d = inRay->d.reflect(n);
			brdf->wt.d = d;
		}
		else
		{
			if (rou < P)
			{
				brdf->fr = RP;
				brdf->wt.d = Vector3(0.0);
			}
			else
			{
				brdf->wr.d = Vector3(0.0);
				brdf->wt.d = d;
				brdf->ft = TP;
			}
		}
		
		brdf->wt.o = x;
	}

	//inRay->o = x;
	//inRay->d = d;
	
	return obj->emissive;
}
#endif

#if 1
__global__ void gather_paths_kernel(float* pixels, Camera* camera, Sphere* spheres, PrecomputedBRDF* pbrdf, curandState* states, int width, int height, int pathlength, int niter, int stream_offset)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y + stream_offset;
	int idx = y*blockDim.x*THREAD_X+ x;

	curandState localState;
#if 1
	__shared__ extern Sphere local_spheres[];
	/*__shared__ Vector3 start;
	__shared__ Vector3 campos;
	__shared__ Vector3 cx;
	__shared__ Vector3 cy;*/
	__shared__ Camera localCamera;

	if (threadIdx.x == 0)
	{
		localCamera = *camera;
	}

	
	if ( threadIdx.x < 11)
	{
		local_spheres[threadIdx.x] = spheres[threadIdx.x];
	}

	__syncthreads();
#endif
	localState = states[idx];
	Vector3 rad(0, 0, 0);

	float2 r;

	r.x = curand_uniform(&localState);
	r.y = curand_uniform(&localState);

	float r1 = 2.0f*r.x;
	float r2 = 2.0f*r.y;
	float dx = r1 < 1.0f ? __fsqrt_ru(r1) - 1.0f : 1.0f - __fsqrt_ru(2.0f - r1);
	float dy = r2 < 1.0f ? __fsqrt_ru(r2) - 1.0f : 1.0f - __fsqrt_ru(2.0f - r2);
	Vector3 d = localCamera.start + localCamera.right*(0.5f + dx + x)*localCamera.pixelDX + localCamera.up*(0.5f + dy + y)*localCamera.pixelDY;

	Ray local_ray;
	//local_ray.o = campos;
	//local_ray.d = (d - campos).normalise();

	local_ray.o = localCamera.position;
	local_ray.d = (d - localCamera.position).normalise();
	
	int obj_id;
	BRDFParams f;//(1.0, 1.0, 1.0);
	
	f.fd = Vector3(1.0,1.0,1.0);
	f.fr = Vector3(1.0,1.0,1.0);
	f.ft = Vector3(1.0,1.0,1.0);
	f.wd = local_ray;
	f.wr.d = Vector3(0.0);
	f.wt.d = Vector3(0.0);

	//Evaluate a path iteratively rather than recursively
	for (int n = 0; n < pathlength; n++)
	{
		BRDFParams brdf;
		brdf.wd.d = Vector3(0.0);
		brdf.wr.d = Vector3(0.0);
		brdf.wt.d = Vector3(0.0);
		brdf.fd = Vector3(1.0,1.0,1.0);
		brdf.fr = Vector3(1.0,1.0,1.0);
		brdf.ft = Vector3(1.0,1.0,1.0);
		
		Vector3 local_radiance = radiance(&f.wd, &brdf, local_spheres, pbrdf, &localState, obj_id, n)
			+ brdf.fr*radiance(&f.wr, &brdf, local_spheres, pbrdf, &localState, obj_id, n)
			+ brdf.ft*radiance(&f.wt, &brdf, local_spheres, pbrdf, &localState, obj_id, n);
		
		rad = rad + f.fd*local_radiance;

		f.fd = f.fd*brdf.fd;	
		f.fr = brdf.fr;	
		f.ft = brdf.ft;	

		f.wd = brdf.wd;
		f.wr = brdf.wr;
		f.wt = brdf.wt;
	}

	states[idx] = localState;
	rad = rad*(1.0f /niter);
	pixels[y*width * 4 + 4 * x]  += (float)rad[0];
	pixels[y*width * 4 + 4 * x + 1] += (float)rad[1];
	pixels[y*width * 4 + 4 * x + 2] += (float)rad[2];
}
#endif

#if 1
__device__ Vector3 radiance(Ray * inRay, Vector3 * brdf, Triangle * triangles, int meshsize, Material * materials, curandState * state)
{
	Vector3 f;
	float t;
	Triangle *obj;

	bool hit = intersect(inRay, &t, triangles, meshsize, &obj);

	if (!hit)
	{
		*brdf = f;
		return f;
	}

	Material *mat = materials + obj->MaterialIndex;
	Vector3 x = inRay->o + inRay->d*t;
	Vector3 e1 = obj->Vertices[1].Position - obj->Vertices[0].Position;
	Vector3 e2 = obj->Vertices[2].Position - obj->Vertices[0].Position;
	Vector3 n = e1%e2;
	n.normalise();
	Vector3 nl = n.dot(inRay->d) < 0.0 ? n : n*-1.0f;

	*brdf = mat->Kd;

	//float4 r = curand_uniform4(state);
	//float4 r;
	float2 r;
	r.x = curand_uniform(state);
	r.y = curand_uniform(state);
	//r.z = curand_uniform(state);
	//r.w = curand_uniform(state);
	
	float r1 = 2.0*M_PI*r.x;
	float r2 = r.y;
	float r2s = __dsqrt_ru(r2);

	Vector3 w = nl;
	Vector3 u = (fabs(w[0])>.1 ? Vector3(0, 1) : Vector3(1));
	u = u%w;
	//u.normalise();
	Vector3 v = w%u;
	Vector3 d = u*cosf(r1)*r2s + v*sinf(r1)*r2s + w*__dsqrt_ru(1.0 - r2);
	d.normalise();

	//Vector3 s(5, 5, 5);
	Vector3 refl_vec = d.reflect(w);
	float phong = refl_vec.dot(inRay->d);

	*brdf = mat->Kd*d.dot(w) + mat->Ks*powf(phong, 50);

	/*if (obj->refl == SPECULAR || obj->refl == REFRACTIVE)
	{
		d = inRay->d.reflect(w);
	}

	float max_refl = (*brdf)[0] > (*brdf)[1] && (*brdf)[0] > (*brdf)[2] ? (*brdf)[0] : (*brdf)[1] > (*brdf)[2] ? (*brdf)[1] : (*brdf)[2];

	if (obj->refl == REFRACTIVE && r.z < max_refl)
	{
		float into = n.dot(nl) > 0 ? 1.0 : -1.0;
		float nt = 1.0;
		float nc = 1.5;
		float nnt = into > 0.0 ? nt / nc : nc / nt;
		float ddn = inRay->d.dot(nl);
		float cos2t = 1.0 - nnt*nnt*(1.0 - ddn*ddn);

		if (cos2t < 0.0) {
			inRay->o = x;
			inRay->d = d;
			return obj->emissive;
		}

		d = inRay->d*nnt - n*into*(ddn*nnt + __sqrt_ru(cos2t));
		d.normalise();
	}*/

	inRay->o = x;
	inRay->d = d;

	return mat->Ke;
}
#endif

#if 1
__global__ void gather_paths_kernel(float * pixels, Triangle * triangles, int meshsize, Material * materials, curandState * states, int width, int height, int pathlength, int niter, int stream_offset)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y + stream_offset;
	int idx = y*blockDim.x*THREAD_X + x;

	curandState localState;
#if 1
	__shared__ Vector3 start;
	__shared__ Vector3 campos;
	__shared__ Vector3 cx;
	__shared__ Vector3 cy;

	float aspectratio = (float)width / height;
	float pixelDX = aspectratio*5.0f / width;
	float pixelDY = 1.0f*5.0f / height;

	if (threadIdx.x == 0)
	{
		campos = Vector3(0, 0, 0);
		Vector3 camdir = Vector3(0, -0.0, -1);
		camdir.normalise();
		cx = Vector3(1.0f);
		cy = cx%camdir;
		cy.normalise();
		start = (campos + camdir) - (cx*aspectratio + cy)*0.5f;
	}

	__syncthreads();
#endif
	localState = states[idx];
	Vector3 rad(0, 0, 0);

	//float4 r = curand_uniform4(&localState);
	//float4 r;
	float2 r;
	r.x = curand_uniform(&localState);
	r.y = curand_uniform(&localState);
	//r.z = curand_uniform(&localState);
	//r.w = curand_uniform(&localState);
	
	float r1 = 2.0f*r.x;
	float r2 = 2.0f*r.y;
	float dx = r1 < 1.0 ? __dsqrt_ru(r1) - 1.0 : 1.0 - __dsqrt_ru(2.0 - r1);
	float dy = r2 < 1.0 ? __dsqrt_ru(r2) - 1.0 : 1.0 - __dsqrt_ru(2.0 - r2);
	//states[idx] = localState;
	Vector3 d = start + cx*(0.5 + dx + x)*pixelDX + cy*(0.5 + dy + y)*pixelDY;

	Ray local_ray;
	local_ray.o = campos;
	local_ray.d = (d - campos).normalise();

	Vector3 brdf;
	Vector3 f(1.0, 1.0, 1.0);

	//Evaluate a path iteratively rather than recursively
	for (int n = 0; n < pathlength; n++)
	{
		rad = rad + f*radiance(&local_ray, &brdf, triangles, meshsize, materials, &localState);
		f = f*brdf;
	}

	states[idx] = localState;
	rad = rad*(1.0 / niter);
	pixels[y*width * 4 + 4 * x] += rad[0];
	pixels[y*width * 4 + 4 * x + 1] += rad[1];
	pixels[y*width * 4 + 4 * x + 2] += rad[2];
	
	return;
}
#endif

__global__ void pixels_tone_kernel(float* pixels, int width, int heighti, int stream_offset)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y + stream_offset;
	
	float r = pixels[y*width * 4 + 4 * x];
	float g = pixels[y*width * 4 + 4 * x + 1];
	float b = pixels[y*width * 4 + 4 * x + 2];

	float p = 1.0f/2.2f;

	pixels[y*width * 4 + 4 * x] = powf(r,p) ;
	pixels[y*width * 4 + 4 * x + 1] = powf(g,p);
	pixels[y*width * 4 + 4 * x + 2] = powf(b,p);
}

#if 1
float pathtrace(float * pixels, Triangle * triangles, int meshsize, Material * materials, curandState * states, int width, int height, int pathlength, cudaStream_t * streams, int nstreams, int niters, int stream_offset)
{
	dim3 threadsPerBlock(THREAD_X, THREAD_Y);
	dim3 grid_size(width / threadsPerBlock.x, height / threadsPerBlock.y);

	dim3 grid_size_per_stream(grid_size.x, (grid_size.y / nstreams + 0.5f));

	for (int i = 0; i < nstreams; i++)
	{
		int offset = i*stream_offset;
		gather_paths_kernel <<<grid_size_per_stream, threadsPerBlock, 0, streams[i] >>>(pixels, triangles, meshsize, materials, states, width, height, pathlength, niters, offset);
	}

	return 0.0f;
}
#endif

__global__ void setup_rng_state_kernel(curandState* state, unsigned int seed)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	int idx = y*blockDim.x*THREAD_X + x;

	curand_init(1234, idx, 0, &state[idx]);
}

#if 1
float pathtrace(float* pixels, Camera* camera, Sphere* spheres, PrecomputedBRDF* brdf, curandState* states, int width, int height, int pathlength, cudaStream_t *streams, int nstreams, int niters, int stream_offset)
{
	dim3 threadsPerBlock(THREAD_X,THREAD_Y);
	dim3 grid_size(width/threadsPerBlock.x,height/threadsPerBlock.y);
	
	dim3 grid_size_per_stream(grid_size.x, (grid_size.y/nstreams + 0.5f));
	
	for (int i = 0; i < nstreams; i++)
	{
		int offset = i*stream_offset;
		gather_paths_kernel<<<grid_size_per_stream, threadsPerBlock, 11*sizeof(Sphere), streams[i]>>>(pixels, camera, spheres, brdf, states, width, height, pathlength, niters, offset);
	}

	return 0.0f;
}
#endif

void initRNG(curandState** states, int width, int height)
{
	dim3 threadsPerBlock(THREAD_X, THREAD_Y);
	dim3 grid_size(width / threadsPerBlock.x, height / threadsPerBlock.y);

	cuda_Call(cudaMalloc(states, width*height*sizeof(curandState)));
	setup_rng_state_kernel<<<grid_size, threadsPerBlock>>>(*states, time(NULL));
	CUDACheckError();
}

__global__ void initCamera_kernel(Camera* camera, int width, int height)
{
	camera->init(width, height);
}


void initCamera(Camera* camera, int width, int height)
{
	initCamera_kernel<<<1,1>>>(camera, width, height);
	CUDACheckError();
}
