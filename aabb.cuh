#pragma once

#include "ray.cuh"
#include "vector3f.cuh"

struct AABB
{
	public:
		Vector3 min;
		Vector3 max;

		__host__ __device__ AABB() {;}

		__host__ __device__ AABB(Vector3 &_min, Vector3 &_max):
			min(_min), max(_max) {;}

		__device__ float intersect(const Ray* r);
};
