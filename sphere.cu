#include <math.h>
#include "sphere.cuh"

__device__
float Sphere::intersect(const Ray* r)
{
	Vector3 op = position - r->o;
	float t, eps = 1.0e-1;
	float b = op.dot(r->d);
	float det = b*b - op.dot(op) + radius*radius;

	if (det < 0 ) 
		return 0;
	else 
		det = __dsqrt_ru(det);

	return (t=b-det) > eps ? t : ((t=b+det)>eps ? t : 0);
}
