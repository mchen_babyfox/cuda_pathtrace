#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CUDA_API_PER_THREAD_DEFAULT_STREAM

#include <cuda_runtime.h>
#include <curand_kernel.h>
#include "cuda_call.h"
#include "vector3f.cuh"
#include "sphere.cuh"
#include "triangle.cuh"
#include "material.h"
//#include "pathvertex.cuh"
#include "camera.cuh"
#include "precomputedbrdf.cuh"

#define THREAD_X 16
#define THREAD_Y 16

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

//__constant__ __device__ Sphere dev_spheres[9];

inline float clamp(float x){ return x<0 ? 0 : x>1 ? 1 : x; } 

inline int toInt(float x){ return int(pow(clamp(x),1/2.2)*255+.5f); }

//__device__ bool intersect(Ray* ray, float* t, Sphere* spheres, int n, int* result);

//__device__ bool intersect(Ray* ray, float* t, Triangle* triangles, int n, Triangle **result);

//__device__ Vector3 radiance(Ray* inRay, Vector3* outColour, Sphere* spheres, curandState* state);

//__device__ Vector3 radiance(Ray* inRay, Vector3* brdf, Triangle* triangles, int meshsize, Material *materials, curandState* state);

__global__ void gather_paths_kernel(float* pixels, Camera* camera, Sphere* spheres, PrecomputedBRDF* brdf, curandState* states, int width, int height, int pathlength, int niter, int stream_offset);

__global__ void gather_paths_kernel(float* pixels, Triangle* triangles, int meshsize, Material *materials, curandState* states, int width, int height, int pathlength, int niter, int stream_offset);

float pathtrace(float* pixels, Camera* camera, Sphere* spheres, PrecomputedBRDF* brdf, curandState* states, int width, int height, int pathlength, cudaStream_t *streams, int nstreams, int niters, int stream_offset);

float pathtrace(float* pixels, Triangle *triangles, int meshsize, Material *materials, curandState* states, int width, int height, int pathlength, cudaStream_t *streams, int nstreams, int niters, int stream_offset);

__global__ void pixels_tone_kernel(float* pixels, int width, int height, int stream_offset);

__global__ void setup_rng_state_kernel(curandState* state, unsigned int seed);

void initRNG(curandState** states, int width, int height);

__global__ void initCamera_kernel(Camera* camera, int width, int height);

void initCamera(Camera* camera, int width, int height);
