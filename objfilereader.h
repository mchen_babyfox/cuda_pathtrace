#pragma once

//#include <Windows.h>
#include "triangle.cuh"
#include "material.h"

int importOBJMesh(const char* filename, Triangle** triangles, const Material *materials, int nMaterials);
int importMTL(const char* filename, Material** materials);
