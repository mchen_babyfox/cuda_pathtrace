#include <stdio.h>

#if defined(_WINDOWS) || defined(_WIN32)
#include <windows.h>
#endif

#include <GL/glew.h>
#include <cuda_gl_interop.h>

#include "cuda_call.h"
#include "glvertexbufferobject.h"

GLVertexBufferObject::~GLVertexBufferObject()
{
	FreeBuffer();
}

void GLVertexBufferObject::CreateVBO(size_t bytes, void *data)
{
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, bytes, data, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GLVertexBufferObject::RegisterCudaGraphicsResource(cudaGraphicsResource ** pRes, unsigned int gfx_res_flags)
{
	cuda_Call(cudaGraphicsGLRegisterBuffer(pRes, vbo, gfx_res_flags));
}

void GLVertexBufferObject::FreeBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glDeleteBuffers(1, &vbo);
}
