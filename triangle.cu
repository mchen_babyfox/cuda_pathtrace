#include <cuda_runtime.h>
#include "triangle.cuh"

__device__
float Triangle::intersect(const Ray *r)
{
	float t = 1.0e5;
	
	Vector3 e1 = Vertices[1].Position - Vertices[0].Position;
	Vector3 e2 = Vertices[2].Position - Vertices[0].Position;
	Vector3 P = r->d%e2;

	float inv_det = 1.0f/(e1.dot(P));
	
	Vector3 T = r->o - Vertices[0].Position;

	float u = T.dot(P)*inv_det;

	if (u < 0.0f || u > 1.0f) return t;

	Vector3 Q = T%e1;

	float v = r->d.dot(Q)*inv_det;

	if (v < 0.0f || u + v > 1.0f) return t;
	
	t = e2.dot(Q)*inv_det;

	if (t > 0.0f)
	{
		return t;	
	}

	return 1e5;
}
