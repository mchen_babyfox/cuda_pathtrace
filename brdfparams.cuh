#pragma once

#include "vector3f.cuh"
#include "ray.cuh"

struct BRDFParams{
	Ray wd;
	Ray wr;
	Ray wt;
	Vector3 fd;
	Vector3 fr;
	Vector3 ft;
};
