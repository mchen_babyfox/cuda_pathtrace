#pragma once

struct Vector3 {
	public:
		double2 xy;
		double2	zw;

		__host__ __device__ Vector3(double vx = 0, double vy = 0, double vz = 0);
		__device__ Vector3 operator+ (const Vector3& op);
		__device__ Vector3 operator- (const Vector3& op);
		__device__ Vector3 operator* (double s);
		__device__ Vector3 operator* (const Vector3& op);
		__device__ Vector3 operator% (const Vector3& op);
		__device__ double operator[] (int n);

		__device__ double dot (const Vector3& op);
		__device__ double norm();
		__device__ Vector3 reflect(Vector3& n);
		__device__ Vector3& normalise();
};
