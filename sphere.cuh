#pragma once

#include "ray.cuh"
#include "vector3f.cuh"

enum REFL_TYPE {
	DIFFUSE = 0x1,
	SPECULAR = 0x1<<1,
	REFRACTIVE = 0x1<<2,
	BRDF = 0x1<<3
};

struct Sphere
{
	public:
		Vector3 colour;
		Vector3 emissive;
		Vector3 position;
		float radius;
		REFL_TYPE refl;
		
		__host__ __device__ Sphere() {;}
		__host__ __device__ Sphere(float r, Vector3 p, Vector3 e, Vector3 c, REFL_TYPE _refl):
			colour(c), emissive(e), position(p), radius(r), refl(_refl) {}

		__device__ float intersect(const Ray* r);
};
