#pragma once

//#include "vector3.cuh"

struct Camera
{
	Vector3 position; //position of the camera
	Vector3 right; //direction of the right axis
	Vector3 up; //direction of the up axis
	Vector3 view; //viewing direction of the camera;
	Vector3 start; //Position of the "top left corner of the image plane"
	float aspectratio;
	float pixelDX;
	float pixelDY;

	__device__ Camera(int width = 1280, int height = 720)
	{
		init(width, height);
	}

	__device__ void init(int width, int height)
	{
		position = Vector3(0.0f,0.0f,0.f);
		
		view = Vector3(0.0f,0.0f,-1.0f);

		right = Vector3(1.0f);
		
		up = right%view;

		up.normalise();

		aspectratio = (float)width/height;

		pixelDX = aspectratio/width;
		pixelDY = 1.0f/height;

		start = (position + view) - (right*aspectratio + up)*0.5f;
	}
};
