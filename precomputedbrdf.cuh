#pragma once

#include "vector3f.cuh"

#define BRDF_SAMPLING_RES_THETA_H       90
#define BRDF_SAMPLING_RES_THETA_D       90
#define BRDF_SAMPLING_RES_PHI_D         360

#define RED_SCALE (1.0f/1500.0f)
#define GREEN_SCALE (1.15f/1500.0f)
#define BLUE_SCALE (1.66f/1500.0f)

#ifndef M_PI
#define M_PI	3.1415926535897932384626433832795
#endif

struct PrecomputedBRDF 
{
private:
	float* Dev_BRDFTable;	//BRDF table stored on gpu device
	
	__device__ int ThetaHalfIndex(float theta_half);
	__device__ int ThetaDiffIndex(float theta_diff);
	__device__ int PhiDiffIndex(float phi_diff);
	__device__ float4 StdCoordsToHalfDiffCoords(float4 in);

public:
	PrecomputedBRDF();
	~PrecomputedBRDF();

	__host__ bool LoadBRDFFromFile(const char* filename);

	__device__ Vector3 LookupBRDF(float theta_in, float phi_in, float theta_out, float phi_out);
};
