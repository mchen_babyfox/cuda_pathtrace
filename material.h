/*---------------------------------------------------------------------
*
* Copyright © 2017  Minsi Chen
* E-mail: min.game@gmail.com
*
---------------------------------------------------------------------*/
#pragma once

#include <stdlib.h>

#include "vector3f.cuh"

typedef Vector3 Colour;

#if 0
class Texture
{
public:
	enum TEXUNIT
	{
		TEXUNIT_DIFFUSE = 0,
		TEXUNIT_NORMAL = 1
	};
public:
	unsigned int	mWidth;
	unsigned int    mHeight;
	unsigned int    mChannels;
	unsigned char*	mImage;

	Texture()
	{
		mImage = NULL;
	}

	~Texture()
	{
		if (mImage) delete[] mImage;
	}

	Colour GetTexelColour(double u, double v)
	{
		Colour colour;

		int dx = u*mWidth;
		int dy = v*mHeight;
		int texel_stride = mChannels;
		int row_stride = mWidth;
		int texel_loc = (dx*texel_stride) + dy*row_stride*texel_stride;

		unsigned char* comp = mImage + texel_loc;

		colour[0] = *(comp) / 255.0f;
		colour[1] = *(comp + 1) / 255.0f;
		colour[2] = *(comp + 2) / 255.0f;

		return colour;
	}
};
#endif

class Material
{
	public:
		//char Name[16];
		Colour Ka;
		Colour Kd;
		Colour Ks;
		Colour Ke;
		unsigned long Name;
		//int Ns;
		//float Ni;

	public:
		
		Material(){;}
		~Material(){;}
};

