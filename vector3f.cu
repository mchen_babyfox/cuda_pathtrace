#include <math.h>
#include "vector3f.cuh"

__host__ __device__
Vector3::Vector3(float vx, float vy, float vz)
{
	v = make_float3(vx, vy, vz);
}

__device__
Vector3 Vector3::operator+ (const Vector3& op) const
{
	return Vector3( __fadd_rn(v.x, op.v.x), __fadd_rn(v.y, op.v.y), __fadd_rn(v.z,op.v.z));
}

__device__
Vector3 Vector3::operator- (const Vector3& op) const
{
	return Vector3( __fsub_rn(v.x, op.v.x), __fsub_rn(v.y, op.v.y), __fsub_rn(v.z, op.v.z));
}

__device__
Vector3 Vector3::operator* (float s) const
{
	return Vector3( __fmul_rn(v.x, s), __fmul_rn(v.y, s), __fmul_rn(v.z, s));
}

__device__
Vector3 Vector3::operator* (const Vector3& op) const
{
	return Vector3( __fmul_rn(v.x, op.v.x), __fmul_rn(v.y, op.v.y), __fmul_rn(v.z, op.v.z));
}

__device__
Vector3 Vector3::operator% (const Vector3& op) const
{
	return Vector3(__fmaf_rn(v.y, op.v.z, __fmaf_rn(-v.z, op.v.y, 0)),
			__fmaf_rn(v.z, op.v.x, __fmaf_rn(-v.x, op.v.z, 0)),
			__fmaf_rn(v.x, op.v.y, __fmaf_rn(-v.y, op.v.x, 0)));
}

__device__
float Vector3::operator[] (int n)
{
	return n == 0 ? v.x : n > 1 ? v.z : v.y;
}

__device__
float Vector3::dot (const Vector3& op) const
{
	return	__fmaf_rn(v.x, op.v.x, __fmaf_rn(v.y, op.v.y, __fmaf_rn(v.z, op.v.z, 0)));
}

__device__ Vector3 Vector3::rotate(const Vector3& axis, float angle) const
{
	Vector3 result;

	float temp;
	//double cross[3];
	float cos_ang = cosf(angle);
	float sin_ang = sinf(angle);

	result = (*this)*cos_ang;
		
	temp = axis.dot(*this);
	temp = temp*(1.0f - cos_ang);

	result = result + axis*temp;
	
	Vector3 vxa = axis%(*this);

	//cross_product(axis, vector, cross);

	result = result + vxa*sin_ang;

	//out[0] += cross[0] * sin_ang;
	//out[1] += cross[1] * sin_ang;
	//out[2] += cross[2] * sin_ang;

	return result;
}

__device__
Vector3 Vector3::reflect(Vector3& n)
{
	return *this-n*2.0*n.dot(*this);
}

__device__
float Vector3::norm ()
{
	//float p = __fmaf_rn(v.x, v.x, __fmaf_rn(v.y, v.y, __fmaf_rn(v.z, v.z, 0)));
	return norm3df(v.x, v.y, v.z);//__dsqrt_rn(p);
}
	
__device__
Vector3& Vector3::normalise ()
{
	*this = *this*rnorm3df(v.x,v.y,v.z);
	return *this;
}
