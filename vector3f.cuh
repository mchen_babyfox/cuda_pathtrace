#pragma once

struct Vector3{
	public:
		float3 v;

		__host__ __device__ Vector3(float vx = 0, float vy = 0, float vz = 0);
		__device__ Vector3 operator+ (const Vector3& op) const;
		__device__ Vector3 operator- (const Vector3& op) const;
		__device__ Vector3 operator* (float s) const;
		__device__ Vector3 operator* (const Vector3& op) const;
		__device__ Vector3 operator% (const Vector3& op) const;
		__device__ float operator[] (int n);
		
		__device__ float dot (const Vector3& op) const;
		__device__ Vector3 rotate(const Vector3& axis, float angle) const;
		__device__ float norm();
		__device__ Vector3 reflect(Vector3& n);
		__device__ Vector3& normalise();
};
