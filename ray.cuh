#pragma once

#include "vector3f.cuh"

struct Ray
{
	public:
		Vector3 o;
		Vector3 d;
		
		__host__ __device__ Ray() {;}
		__host__ __device__ Ray( Vector3 _o, Vector3 _d) : o(_o), d(_d) {}
};
