#include <stdio.h>
#include <memory.h>
#include <cuda_runtime.h>
#include "precomputedbrdf.cuh"
#include "cuda_call.h"

PrecomputedBRDF::PrecomputedBRDF()
{
	Dev_BRDFTable = NULL;
}

PrecomputedBRDF::~PrecomputedBRDF()
{
	printf("Clearing BRDF table\n");
	if(Dev_BRDFTable) cuda_Call(cudaFree(Dev_BRDFTable));
}

__device__
int PrecomputedBRDF::ThetaHalfIndex(float theta_half)
{
	if (theta_half <= 0.0)
		return 0;
	float theta_half_deg = ((theta_half / (M_PI * 0.5f))*BRDF_SAMPLING_RES_THETA_H);
	float temp = theta_half_deg*BRDF_SAMPLING_RES_THETA_H;
	temp = sqrtf(temp);

	int ret_val = (int)temp;
	if (ret_val < 0) ret_val = 0;
	if (ret_val >= BRDF_SAMPLING_RES_THETA_H)
		ret_val = BRDF_SAMPLING_RES_THETA_H - 1;
	
	return ret_val;
}

__device__
int PrecomputedBRDF::ThetaDiffIndex(float theta_diff)
{
	int tmp = int(theta_diff / (M_PI * 0.5f) * BRDF_SAMPLING_RES_THETA_D);
	if (tmp < 0)
		return 0;
	else if (tmp < BRDF_SAMPLING_RES_THETA_D - 1)
		return tmp;
	else
		return BRDF_SAMPLING_RES_THETA_D - 1;

}

__device__
int PrecomputedBRDF::PhiDiffIndex(float phi_diff)
{
	// Because of reciprocity, the BRDF is unchanged under
	// phi_diff -> phi_diff + M_PI
	if (phi_diff < 0.0)
		phi_diff += M_PI;

	// In: phi_diff in [0 .. pi]
	// Out: tmp in [0 .. 179]
	int tmp = int(phi_diff / M_PI * BRDF_SAMPLING_RES_PHI_D / 2);
	if (tmp < 0)
		return 0;
	else if (tmp < BRDF_SAMPLING_RES_PHI_D * 0.5f - 1)
		return tmp;
	else
		return BRDF_SAMPLING_RES_PHI_D * 0.5f - 1;
}

__device__
float4 PrecomputedBRDF::StdCoordsToHalfDiffCoords(float4 in)
{
	// compute in vector
	float in_vec_z = cosf(in.x);
	float proj_in_vec = sinf(in.x);
	float in_vec_x = proj_in_vec*cosf(in.y);
	float in_vec_y = proj_in_vec*sinf(in.y);
	Vector3 vec_in(in_vec_x, in_vec_y, in_vec_z);
	vec_in.normalise();
	
	// compute out vector
	float out_vec_z = cosf(in.z);
	float proj_out_vec = sinf(in.z);
	float out_vec_x = proj_out_vec*cosf(in.w);
	float out_vec_y = proj_out_vec*sinf(in.w);
	Vector3 vec_out(out_vec_x, out_vec_y, out_vec_z);
	vec_out.normalise();
	
	// compute halfway vector
	//float half_x = (in_vec_x + out_vec_x)*0.5f;
	//float half_y = (in_vec_y + out_vec_y)*0.5f;
	//float half_z = (in_vec_z + out_vec_z)*0.5f;
	Vector3 vec_half = (vec_in + vec_out)*0.5f;
	//Vector3 vec_half(half_x, half_y, half_z);
	vec_half.normalise();

	//float4 layout: x = theta_half, y = phi_half, z = theta_diff, w = phi_diff
	float4 theta_fi_half_diff;
	// compute  theta_half, fi_half
	theta_fi_half_diff.x = acosf(vec_half[2]);
	theta_fi_half_diff.y = atan2f(vec_half[1], vec_half[0]);


	Vector3 bi_normal( 0.0, 1.0, 0.0 );
	Vector3 normal( 0.0, 0.0, 1.0 );
	Vector3 temp;
	Vector3 diff;

	// compute diff vector
	temp = vec_in.rotate(normal, -theta_fi_half_diff.y);
	diff = temp.rotate(bi_normal, -theta_fi_half_diff.x);

	// compute  theta_diff, fi_diff	
	theta_fi_half_diff.z = acosf(diff[2]);
	theta_fi_half_diff.w = atan2f(diff[1], diff[0]);

	return theta_fi_half_diff;
}

__device__
Vector3 PrecomputedBRDF::LookupBRDF(float theta_in, float phi_in, float theta_out, float phi_out)
{
	// Convert to halfangle / difference angle coordinates
	//double theta_half, fi_half, theta_diff, fi_diff;
	
	//float4 layout x = theta_in, y = phi_in, z = theta_out, w = phi_out;
	float4 theta_fi;
	theta_fi.x = theta_in;
	theta_fi.y = phi_in;
	theta_fi.z = theta_out;
	theta_fi.w = phi_out;

	float4 theta_fi_half_diff;

	theta_fi_half_diff = StdCoordsToHalfDiffCoords(theta_fi);
		//theta_half, fi_half, theta_diff, fi_diff);


	// Find index.
	// Note that phi_half is ignored, since isotropic BRDFs are assumed
	/*int ind = PhiDiffIndex(fi_diff) +
		ThetaDiffIndex(theta_diff) * BRDF_SAMPLING_RES_PHI_D / 2 +
		ThetaHalfIndex(theta_half) * BRDF_SAMPLING_RES_PHI_D / 2 *
		BRDF_SAMPLING_RES_THETA_D;*/
	int ind = PhiDiffIndex(theta_fi_half_diff.w) +
		ThetaDiffIndex(theta_fi_half_diff.z) * BRDF_SAMPLING_RES_PHI_D / 2 +
		ThetaHalfIndex(theta_fi_half_diff.x) * BRDF_SAMPLING_RES_PHI_D / 2 *
		BRDF_SAMPLING_RES_THETA_D;


	Vector3 out;

	out.v.x = Dev_BRDFTable[ind] * RED_SCALE;
	out.v.y = Dev_BRDFTable[ind + BRDF_SAMPLING_RES_THETA_H*BRDF_SAMPLING_RES_THETA_D*BRDF_SAMPLING_RES_PHI_D / 2] * GREEN_SCALE;
	out.v.z = Dev_BRDFTable[ind + BRDF_SAMPLING_RES_THETA_H*BRDF_SAMPLING_RES_THETA_D*BRDF_SAMPLING_RES_PHI_D] * BLUE_SCALE;
	
	//if (red_val < 0.0 || green_val < 0.0 || blue_val < 0.0)
		//fprintf(stderr, "Below horizon.\n");

	return out;
}

__host__
bool PrecomputedBRDF::LoadBRDFFromFile(const char* filename)
{
	FILE *f = fopen(filename, "rb");
	if (!f)
		return false;

	size_t byte_read;

	int dims[3];
	
	byte_read = fread(dims, sizeof(int), 3, f);
	
	int n = dims[0] * dims[1] * dims[2];
	if (n != BRDF_SAMPLING_RES_THETA_H *
		 BRDF_SAMPLING_RES_THETA_D *
		 BRDF_SAMPLING_RES_PHI_D / 2) 
	{
		fprintf(stderr, "Dimensions don't match\n");
		fclose(f);
		return false;
	}

	double* brdf = (double*) malloc (sizeof(double)*3*n);
	float* fbrdf = (float*) malloc (sizeof(float)*3*n);
	byte_read = fread(brdf, sizeof(double), 3*n, f);
	
	cuda_Call(cudaMalloc(&Dev_BRDFTable, 3*n*sizeof(float)));
	
	for ( int i = 0; i < 3*n; i++ )
	{
		fbrdf[i] = (float)brdf[i];
	}

	cuda_Call(cudaMemcpy(Dev_BRDFTable, fbrdf, 3*n*sizeof(float), cudaMemcpyHostToDevice));

	free(brdf);
	free(fbrdf);

	fclose(f);
	return true;
}
