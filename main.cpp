#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cuda_gl_interop.h>
#include "cupathtraceargs.h"
#include "cupathtrace.cuh"
#include "glvertexbufferobject.h"
#include "material.h"
#include "objfilereader.h"
#include "camera.cuh"
#include "precomputedbrdf.cuh"

#define THREAD_X 16
#define THREAD_Y 16

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

Sphere* dev_pSpheres = NULL;
Sphere* host_pSpheres = NULL;
Camera* dev_pCamera = NULL;
Camera* host_pCamera = NULL;
PrecomputedBRDF* dev_pBRDF = NULL;
PrecomputedBRDF* host_pBRDF = NULL;


size_t num_spheres = 0;
int num_materials;
int meshsize;
float*	dev_pixels = NULL;
Material* dev_materials = NULL;
Triangle *dev_triangle = NULL;
//curandStatePhilox4_32_10_t* dev_rngstate = NULL;
curandState* dev_rngstate = NULL;

//GL variables
GLuint g_pbo;	//OpenGL pixel buffer object
GLuint g_tex;	//OpenGL texture unit
cudaGraphicsResource *g_cuda_pbo_resource;
cudaGraphicsResource *g_cuda_vbo_resource;
GLVertexBufferObject *g_gl_vbo;

void initGLBuffers(GLFWwindow *win, int width, int height)
{
	int frame_width, frame_height;
	glfwGetFramebufferSize(win, &frame_width, &frame_height);

	printf("GLFW w %d, h %d\n", frame_width, frame_height);

	glGenTextures(1, &g_tex);
	glBindTexture(GL_TEXTURE_2D, g_tex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0,
		GL_RGBA, GL_FLOAT, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenBuffers(1, &g_pbo);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, g_pbo);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, width*height*sizeof(float)*4, 0, GL_STREAM_DRAW);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0); 

	glViewport(0,0, frame_width, frame_height);

	cuda_Call(cudaGraphicsGLRegisterBuffer(&g_cuda_pbo_resource, g_pbo, cudaGraphicsMapFlagsWriteDiscard));
	
	cuda_Call(cudaGraphicsMapResources(1, &g_cuda_pbo_resource, 0));
	size_t num_bytes;
	cuda_Call(cudaGraphicsResourceGetMappedPointer((void**)&dev_pixels, &num_bytes, g_cuda_pbo_resource));
	cuda_Call(cudaMemset(dev_pixels, 0, num_bytes));
	cuda_Call(cudaGraphicsUnmapResources(1, &g_cuda_pbo_resource, 0));
	glEnable(GL_TEXTURE_2D);

	//TODO: This is temporarily here for testing
	//N.B. Must read the material file first
#if 0
	Material *materials = NULL;
	num_materials = importMTL("sibenik.mtl", &materials);

	cuda_Call(cudaMalloc(&dev_materials, num_materials*sizeof(Material)));
	cuda_Call(cudaMemcpy(dev_materials, materials, num_materials*sizeof(Material), cudaMemcpyHostToDevice))

	Triangle *mesh = NULL;
	meshsize = importOBJMesh("sibenik.obj", &mesh, materials, num_materials);

	/*g_gl_vbo = new GLVertexBufferObject();
	g_gl_vbo->CreateVBO(meshsize*sizeof(Triangle), mesh);
	g_gl_vbo->RegisterCudaGraphicsResource(&g_cuda_vbo_resource, cudaGraphicsRegisterFlagsReadOnly);
	*/
	cuda_Call(cudaMalloc(&dev_triangle, meshsize*sizeof(Triangle)));
	cuda_Call(cudaMemcpy(dev_triangle, mesh, meshsize*sizeof(Triangle), cudaMemcpyHostToDevice))

	delete[] mesh;
	
	delete[] materials;
#endif
}

void freeGLBuffers()
{
	printf("Clean up graphics resources.\n");
	
	cuda_Call(cudaGraphicsUnregisterResource(g_cuda_pbo_resource));

	glDeleteBuffers(1, &g_pbo);
	glDeleteTextures(1, &g_tex);

	//delete g_gl_vbo;
}

void initMemAndData(int width, int height)
{
	double scale = 0.05f;
	
	Sphere spheres[] = {//Scene: radius, position, emission, color, material 
		Sphere(1e5*scale, Vector3((1e5) * scale + 50.0, 0.0, 0.0), Vector3(),Vector3(.75*scale,.25*scale,.25*scale),DIFFUSE),//Left 
		Sphere(1e5*scale, Vector3((-1e5) * scale - 50.0 ,0.0, 0.0),Vector3(),Vector3(.25*scale,.25*scale,.75*scale),DIFFUSE),//Rght 
		Sphere(1e5*scale, Vector3(0.0f, 0.0f, -1e5*scale - 200.0),     Vector3(),Vector3(.999, .89,.999),SPECULAR),//Back 
		Sphere(1e5*scale, Vector3(0.0f, 0.0f ,(1e5) * scale + 200.0), Vector3(),Vector3(.75*scale, .75*scale, .75*scale),           DIFFUSE),//Frnt 
		Sphere(1e5*scale, Vector3(0.0f, (1e5)*scale + 40.0, 0.0),    Vector3(),Vector3(.75*scale,.75*scale,.75*scale),DIFFUSE),//Botm 
		Sphere(1e5*scale, Vector3(0.0f,(-1e5)*scale - 40.0,0.0),Vector3(),Vector3(.75*scale,.75*scale,.75*scale),DIFFUSE),//Top 
		Sphere(15, Vector3(-25,-25,-120),       Vector3(),Vector3(0.999,0.999,0.999), BRDF),//Mirr 
		Sphere(15, Vector3(25,-25,-120),       Vector3(),Vector3(0.999,0.999,0.999), BRDF),//Glas 
		Sphere(10, Vector3(-40.0f-10, 0,-120.0f),Vector3(100,100,100),  Vector3(), DIFFUSE), //Light 
		Sphere(10, Vector3(40.0f+10,  0,-120.0f),Vector3(100,100,100),  Vector3(), DIFFUSE), //Light
		Sphere(200, Vector3(0.0, 40+200,-120),Vector3(300,300,300),  Vector3(), DIFFUSE) //Light 
	};

	num_spheres = sizeof(spheres)/sizeof(Sphere);
	
	cuda_Call(cudaMallocHost(&host_pSpheres, num_spheres*sizeof(Sphere)));
	cuda_Call(cudaMalloc(&dev_pSpheres, num_spheres*sizeof(Sphere)));
	cuda_Call(cudaMemcpy(dev_pSpheres, spheres, num_spheres*sizeof(Sphere), cudaMemcpyHostToDevice));
	cuda_Call(cudaMemcpy(host_pSpheres, dev_pSpheres, num_spheres*sizeof(Sphere), cudaMemcpyDeviceToHost));

	cuda_Call(cudaHostAlloc(&host_pCamera, sizeof(Camera), cudaHostAllocMapped));
	//cuda_Call(cudaHostGetDevicePointer(&dev_pCamera, host_pCamera, 0));
	cuda_Call(cudaMalloc(&dev_pCamera, sizeof(Camera)));

	initCamera(dev_pCamera, width, height);
}

void freeMemAndData()
{
	printf("Clean up device resources.\n");
	cuda_Call(cudaFree(dev_pSpheres));
	cuda_Call(cudaFree(dev_materials));
	//cuda_Call(cudaFree(dev_triangle));
	cuda_Call(cudaFreeHost(host_pSpheres));
	cuda_Call(cudaFreeHost(host_pCamera));
	cuda_Call(cudaFree(dev_pCamera));
	cuda_Call(cudaFree(dev_rngstate));
	cuda_Call(cudaFreeHost(host_pBRDF));
}

void displayComputeDeviceProperties(const cudaDeviceProp* prop)
{
	printf("Device name: %s\n", prop->name);
	printf("Compute Capability: %d.%d\n", prop->major, prop->minor);
	printf("Device frequency: %d kHz\n", prop->clockRate);
	printf("Is Multi-GPU: %d\n", prop->isMultiGpuBoard);
	printf("No. MPs: %d\n", prop->multiProcessorCount);
	printf("No. async engines: %d\n", prop->asyncEngineCount);
	printf("Concurrent kernel exec.: %d\n", prop->concurrentKernels);
	printf("\n");
}

void queryComputeCapabilities()
{
	int ngpu_dev;	//#of compute capable gpu

	cuda_Call(cudaGetDeviceCount(&ngpu_dev));
	printf("Found %d Compute GPU(s)\n", ngpu_dev);

	for (int n = 0; n < ngpu_dev; n++)
	{
		cudaDeviceProp prop;

		cuda_Call(cudaGetDeviceProperties(&prop, n));
		displayComputeDeviceProperties(&prop);
	}
	fflush(stdout);
}

void writePPM(const float* pixels, int w, int h)
{
	float *local_pixels = NULL;

	cuda_Call(cudaMallocHost(&local_pixels, sizeof(float)*w*h * 4));

	cuda_Call(cudaMemcpy(local_pixels, pixels, sizeof(float)*w*h * 4, cudaMemcpyDeviceToHost));
#if defined(WINDOWS) || defined(WIN32)
	FILE *f = NULL;
	fopen_s(&f, cuPathTraceArg::Filename, "w");         // Write image to PPM file. 
#else
	FILE *f = fopen(cuPathTraceArg::Filename, "w");         // Write image to PPM file. 
#endif

	fprintf(f, "P3\n%d %d\n%d\n", w, h, 255);
	for (int i = 0; i<w*h * 4; i += 4)
		fprintf(f, "%d %d %d ", toInt(local_pixels[i]), toInt(local_pixels[i + 1]), toInt(local_pixels[i + 2]));

	cuda_Call(cudaFreeHost(local_pixels));
}

void runWithoutDisplay(int width, int height, int pathlength, int nsamples)
{
	dim3 threadsPerBlock(THREAD_X, THREAD_Y);
	dim3 grid_size(width / threadsPerBlock.x, height / threadsPerBlock.y);

	const int nstream = 1;
	cudaStream_t streams[nstream];
	dim3 grid_size_per_stream(grid_size.x, (grid_size.y / nstream + 0.5));
	cudaEvent_t start, stop;
	float elapsedTime = 0.0f;
	int sample_count = 0;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	initMemAndData(width, height);

	cuda_Call(cudaMalloc(&dev_pixels, sizeof(float)*width*height * 4));

#if defined(WINDOWS) || defined(WIN32)
	//cuda_Call(cudaFuncSetCacheConfig(gather_paths_kernel, cudaFuncCachePreferL1));
#endif

	for (int i = 0; i < nstream; i++)
	{
		cuda_Call(cudaStreamCreate(&streams[i]));
	}

	while (sample_count < nsamples)
	{
		cudaEventRecord(start, streams[0]);
		pathtrace(dev_pixels, dev_pCamera, dev_pSpheres, dev_pBRDF, dev_rngstate, width, height, pathlength, streams, nstream, nsamples, threadsPerBlock.y*grid_size_per_stream.y);
		cudaEventRecord(stop, streams[0]);
		cudaEventSynchronize(stop);

		cudaEventElapsedTime(&elapsedTime, start, stop);

		fprintf(stdout, "cuPathtrace %d/%d at %.2fms per sample\r", sample_count + 1, nsamples, elapsedTime);
		sample_count++;
	}

	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	for (int i = 0; i < nstream; i++)
	{
		cuda_Call(cudaStreamDestroy(streams[i]));
	}

	writePPM(dev_pixels, width, height);

	cuda_Call(cudaFree(dev_pixels));
}

int main (int argc, const char** argv)
{
	if (!cuPathTraceArg::ParseCmdline(argc, argv))
	{
		return -1;
	}

	queryComputeCapabilities();

	int width = cuPathTraceArg::Width;
	int height = cuPathTraceArg::Height;

	int niters = cuPathTraceArg::Samples;
	int pathlength = cuPathTraceArg::PathLength;
	float render_scale = cuPathTraceArg::RenderScale;
	int sample_count = 0;

	int render_width = width*render_scale;
	int render_height = height*render_scale;

	initMemAndData(render_width, render_height);

	initRNG(&dev_rngstate, render_width, render_height);

	if (cuPathTraceArg::NoDisplay == 1)
	{
		runWithoutDisplay(render_width, render_height, pathlength, niters);

		freeMemAndData();

		cudaDeviceReset();

		return 0;
	}

	if(!glfwInit()) 
		return -1;

	GLFWwindow* window = glfwCreateWindow(width, height, "cuPathtrace", NULL, NULL);

	if(!window)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	
	glewInit();

	if (!glewIsSupported("GL_VERSION_2_0 GL_ARB_pixel_buffer_object"))
	{
		printf("Required OpenGL extension missing\n");
	}

	initGLBuffers(window, render_width, render_height);

#if defined(WINDOWS) || defined(WIN32)
	//cuda_Call(cudaFuncSetCacheConfig(gather_paths_kernel, cudaFuncCachePreferL1));
#endif

	dim3 threadsPerBlock(THREAD_X, THREAD_Y);
	dim3 grid_size(render_width / threadsPerBlock.x, render_height / threadsPerBlock.y);

	const int nstream = 1;
	cudaStream_t streams[nstream];
	dim3 grid_size_per_stream(grid_size.x, (grid_size.y / nstream + 0.5));
	cudaEvent_t start, stop;
	float elapsedTime = 0.0f;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	for (int i = 0; i < nstream; i++)
	{
		cuda_Call(cudaStreamCreate(&streams[i]));
	}
	

	cuda_Call(cudaHostAlloc(&host_pBRDF, sizeof(PrecomputedBRDF), cudaHostAllocMapped));
	//cuda_Call(cudaMalloc(&g_pBRDF, sizeof(PrecomputedBRDF)));
	cuda_Call(cudaHostGetDevicePointer(&dev_pBRDF, host_pBRDF, 0));
	
	host_pBRDF->LoadBRDFFromFile("../brdfs/brass.binary");

	//cuda_Call(cudaFreeHost(host_pBRDF));
	//cuda_Call(cudaFree(dev_pBRDF));

	/*cuda_Call(cudaGraphicsMapResources(1, &g_cuda_vbo_resource, 0));
	size_t bytes;
	cuda_Call(cudaGraphicsResourceGetMappedPointer((void**)&dev_triangle, &bytes, g_cuda_vbo_resource));*/

	while (!glfwWindowShouldClose(window))
	{
		cuda_Call(cudaGraphicsMapResources(1, &g_cuda_pbo_resource, 0));
		size_t num_bytes;
		cuda_Call(cudaGraphicsResourceGetMappedPointer((void**)&dev_pixels, &num_bytes, g_cuda_pbo_resource));

		if (sample_count < niters)
		{
			cudaEventRecord(start, streams[0]);
			pathtrace(dev_pixels, dev_pCamera, dev_pSpheres, dev_pBRDF, dev_rngstate, render_width, render_height, pathlength, streams, nstream, niters, threadsPerBlock.y*grid_size_per_stream.y);
			cudaEventRecord(stop, streams[0]);
			cudaEventSynchronize(stop);

			cudaEventElapsedTime(&elapsedTime, start, stop);

			char state[64];
			sprintf(state, "cuPathtrace %d/%d at %.2fms per sample\r", sample_count + 1, niters, elapsedTime);
			sample_count++;
			glfwSetWindowTitle(window, state);
		}
		
		cuda_Call(cudaGraphicsUnmapResources(1, &g_cuda_pbo_resource, 0));
	
		glClear(GL_COLOR_BUFFER_BIT);

		glBindTexture(GL_TEXTURE_2D, g_tex);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, g_pbo);

		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, render_width, render_height,
			GL_RGBA, GL_FLOAT, 0);
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, 1.0, 0.0, 1.0, -200.0, 200.0);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, -200.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, -200.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, -200.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -200.0f);
		glEnd();

		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	//cuda_Call(cudaGraphicsUnmapResources(1, &g_cuda_vbo_resource, 0));

	freeMemAndData();

	freeGLBuffers();
	
	glfwTerminate();

	cudaDeviceReset();

	return 0;
}
