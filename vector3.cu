#include <math.h>
#include "vector3.cuh"

__host__ __device__
Vector3::Vector3(double vx, double vy, double vz)
{
	//x = vx; y = vy; z = vz;
	xy = make_double2(vx,vy);
	zw = make_double2(vz,0.0);
}

__device__ 
Vector3 Vector3::operator+ (const Vector3& op)
{
	return Vector3( __dadd_rn(xy.x, op.xy.x), __dadd_rn(xy.y, op.xy.y), __dadd_rn(zw.x,op.zw.x));
}

__device__ 
Vector3 Vector3::operator- (const Vector3& op)
{
	return Vector3( __dsub_rn(xy.x, op.xy.x), __dsub_rn(xy.y, op.xy.y), __dsub_rn(zw.x, op.zw.x));
}

__device__ 
Vector3 Vector3::operator* (double s)
{
	return Vector3( __dmul_rn(xy.x, s), __dmul_rn(xy.y, s), __dmul_rn(zw.x, s));
}

__device__ 
Vector3 Vector3::operator* (const Vector3& op)
{
	return Vector3( __dmul_rn(xy.x, op.xy.x), __dmul_rn(xy.y, op.xy.y), __dmul_rn(zw.x, op.zw.x));
}

__device__ 
Vector3 Vector3::operator% (const Vector3& op)
{
	return Vector3(__fma_rn(xy.y, op.zw.x, __fma_rn(-zw.x, op.xy.y, 0)),
			__fma_rn(zw.x, op.xy.x, __fma_rn(-xy.x, op.zw.x, 0)),
			__fma_rn(xy.x, op.xy.y, __fma_rn(-xy.y, op.xy.x, 0)));
}

__device__
double Vector3::operator[] (int n)
{
	return n == 0 ? xy.x : n > 1 ? zw.x : xy.y;
}

__device__ 
double Vector3::dot (const Vector3& op)
{
	return	__fma_rn(xy.x, op.xy.x, __fma_rn(xy.y, op.xy.y, __fma_rn(zw.x, op.zw.x, 0)));
}

__device__
Vector3 Vector3::reflect(Vector3& n)
{
	return *this-n*2.0*n.dot(*this);
}

__device__ 
double Vector3::norm ()
{
	//double p = __fma_rn(xy.x, xy.x, __fma_rn(xy.y, xy.y, __fma_rn(zw.x, zw.x, 0)));
	return norm3d(xy.x, xy.y, zw.x);//__dsqrt_rn(p);
}
	
__device__ 
Vector3& Vector3::normalise ()
{
	*this = *this*rnorm3d(xy.x,xy.y,zw.x);
	return *this;
}
